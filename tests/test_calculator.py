from pytest import fixture, raises

from calculator.exception import OperatorNotFound
from calculator.service import CalculatorService


@fixture
def calculator_service():
    return CalculatorService()


def test_should_add_two_numbers(calculator_service):
    assert calculator_service.calculate_two_numbers("add", 1, 2) == 3


def test_should_subtract_two_numbers(calculator_service):
    assert calculator_service.calculate_two_numbers("sub", 15, 2) == 13


def test_should_multiply_two_numbers(calculator_service):
    assert calculator_service.calculate_two_numbers("mul", 12, 11) == 132


def test_should_divide_two_numbers(calculator_service):
    assert calculator_service.calculate_two_numbers("div", 99, 3) == 33


def test_should_raise_ZeroDivisionError_when_divide_by_zero(calculator_service):
    with raises(ZeroDivisionError):
        calculator_service.calculate_two_numbers("div", 555, 0)


def test_should_raise_OperatorNotFound_when_divide_by_zero(calculator_service):
    with raises(OperatorNotFound):
        calculator_service.calculate_two_numbers("hello", 6, 9)
