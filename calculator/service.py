from typing import Union

from calculator.calculator import Calculator
from calculator.exception import OperatorNotFound


class CalculatorService:
    def __init__(self):
        self._calculator = Calculator()

    def calculate_two_numbers(self, operator: str, a: int, b: int) -> Union[int, float]:
        if operator == 'add':
            result = self._calculator.add(a, b)
        elif operator == 'sub':
            result = self._calculator.subtract(a, b)
        elif operator == 'mul':
            result = self._calculator.multiply(a, b)
        elif operator == 'div':
            try:
                result = self._calculator.divide(a, b)
            except ZeroDivisionError as e:
                raise e
        else:
            raise OperatorNotFound(operator)
        return result
