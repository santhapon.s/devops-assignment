# DevOps Assignment

## โจทย์
ให้ deploy Python REST API บน AWS โดยมีรายละเอียดดังต่อไปนี้
- clone โค้ดจาก repo นี้ไปยัง repo ของตัวเอง (ควรจะใช้ gitlab ถ้าเป็นไปได้)
- ให้สร้าง account AWS ใหม่ 1 environment
- setup VPC, private subnets, public subnets ตามที่เหมาะสม
- ต้อง deploy ตัว application Fargate ใน private network โดยตัว application [มีรายละเอียดดังนี้](#รายละเอียดตัว-application)
- Application ต้องสามารถ auto scale ได้ (เงื่อนไขคิดเองได้เลย)
- ต้องเชื่อมต่อ Application กับ API Gateway โดยมี[รายละเอียดดังนี้](#รายละเอียด-api-gateway)
- ต้องทำ deployment pipeline โดยมีขั้นตอนตาม[รายละเอียดนี้](#รายละเอียด-deployment-pipeline)
- ให้ส่ง assignment ให้กรรมการตรวจสอบทาง email `santhapon@via.link` โดยแนบรายละเอียดต่อไปนี้
  - URL ของ api gateway
  - วิธีการเข้า AWS ของ project ที่ทำ
  - invitation เข้า Gitlab Repo สำหรับ user @santhapon.s (ต้องให้สิทธิในการ push code เพื่อทำการตรวจด้วย)
  - document อธิบายสิ่งที่ทำไปโดยสังเขป

โดยมี[วิธีการตรวจ assignment ดังนี้](#วิธีการตรวจ-assignment)

## รายละเอียดตัว application
เป็น REST API ที่เขียนด้วยภาษา Python โดยใช้ Framework FastAPI

### การ build และ run
สามารถ build และ run โดยใช้ Docker ด้วยคำสั่ง
```shell
docker build -t devops-assignment:latest .
docker run -p 8080:8080 -it devops-assignment:latest
```
application จะ serve ที่ port 8080
**ควรจะสามารถ deploy โดยไม่แก้ port ถ้าเป็นไปได้**

### การ test
สามารถ test โดยใช้ Docker Image ที่ build ไว้ด้วยคำสั่ง
```shell
docker run -it devops-assignment:latest pytest tests
```
โดยโค้ดที่โจทย์กำหนดให้ ควรจะสามารถผ่านทุก test case โดยไม่ต้องแก้ไขอะไรเพิ่มเติม
**หากมี test case ที่ fail จะมี exit code ไม่เป็น 0**

### endpoints ทั้งหมดของ application

| method | path          | query parameter | body           | curl example                                                                                                                   | response example                      |
|--------|---------------|-----------------|----------------|--------------------------------------------------------------------------------------------------------------------------------|---------------------------------------|
| GET    | /             | -               | -              | curl -X GET -i http://localhost:8080/                                                                                          | {"message":"Hello World"}             |
| GET    | /hello/{name} | -               | -              | curl -X GET -i http://localhost:8080/hello/vialink                                                                             | {"message":"Hello vialink"}           |
| GET    | /company      | name            | -              | curl -X GET -i http://localhost:8080/company?name=vialink                                                                      | {"message":"company 'vialink' found"} |
| POST   | /calculate    | -               | operator, a, b | curl -X POST -i http://localhost:8080/calculate -H "Content-Type: application/json" -d '{"operator": "div", "a": 123, "b": 2}' | {"result":61.5}                       |

## รายละเอียด api gateway
api gateway ต้องมี endpoint ดังต่อไปนี้

| method | endpoint บน api gateway | map ไปที่ endpoint ของ application |
|--------|-------------------------|------------------------------------|
| GET    | /hello                  | /                                  |
| GET    | /serviceA/hello/{name}  | /hello/{name}                      |
| GET    | /serviceA/company       | /company                           |
| POST   | /serviceA/calculate     | /calculate                         |

**โดยที่แต่ละ endpoint ต้องทำงานได้ไม่ต่างจากรัน application บน local (ต่างแค่ URL และไม่ต้องใส่ port)**

## รายละเอียด deployment pipeline
สามารถ implement ด้วยเครื่องมือใด ๆ ของ AWS ได้ โดยมีเงื่อนไขดังนี้
- pipeline จะต้องทำงานเมื่อ push โค้ดเข้าสู่ branch `main`
- pipeline จะต้องทำการ [test application](#การ-test) ก่อน หากมี test case ที่ fail จะต้องยกเลิก pipeline ทันที
- หากการ test ผ่านทั้งหมด ให้ update application บน fargate

## วิธีการตรวจ assignment
- ทดสอบว่า endpoints ทั้งหมดสามารถทำงานได้ผ่าน API Gateway URL ทั้ง success case (status code: 200) และ fail case (status code: 4xx)
- ลอง push โค้ด เพื่อทดสอบว่า pipeline สามารถ update application ได้จริง
- ลอง push โค้ดที่ทำให้ test case fail เพื่อทดสอบว่า pipeline จะถูกยกเลิกเอง
- login เข้า AWS project เพื่อดูโครงสร้างต่าง ๆ ที่ทำไว้ โดยใช้ `document ที่แนบมา` ประกอบ
