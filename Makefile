run:
	docker build -t devops-assignment\:latest .
	docker run -p 8080\:8080 -it devops-assignment\:latest

test:
	docker build -t devops-assignment\:latest .
	docker run -it devops-assignment\:latest pytest tests
