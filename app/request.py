from pydantic import BaseModel


class CalculationRequest(BaseModel):
    operator: str
    a: int
    b: int
