from fastapi import APIRouter, Depends
from starlette.responses import JSONResponse

from app.request import CalculationRequest
from calculator.exception import OperatorNotFound
from calculator.service import CalculatorService

router = APIRouter()


@router.get("/")
async def say_hello():
    return {"message": f"Hello World"}


@router.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}


@router.get("/company")
async def find_company(name: str):
    if name == "vialink":
        return {"message": "company 'vialink' found"}
    return {"message": f"company {name} not found"}


@router.post("/calculate")
async def calculate(request: CalculationRequest, calculator_service: CalculatorService = Depends()):
    try:
        result = calculator_service.calculate_two_numbers(request.operator, request.a, request.b)
    except ZeroDivisionError:
        return JSONResponse(status_code=400, content={"detail": "Cannot divide by 0"})
    except OperatorNotFound as e:
        return JSONResponse(status_code=400, content={"detail": f"Operation '{e}' not found"})

    return {"result": result}
